package ui;

import persistence.model.Category;
import service.CategoryService;

import java.util.List;
import java.util.Scanner;

public class CategoryUi {

    private CategoryService categoryService;

    private Scanner scanner;

    public CategoryUi(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void startCategoryUi() {
        scanner = new Scanner(System.in);

        int option;
        do {
            System.out.println("1.Add category");
            System.out.println("2.View categories");
            System.out.println("3.Delete category");
            System.out.println("4.Add product to category");
            System.out.println("0.Exit");
            option = scanner.nextInt();
            scanner.nextLine();

            switch (option) {
                case 1:
                    addCategoryUi();
                    break;
                case 2:
                    viewCategoriesUi();
                    break;
                case 4:
                    viewAddProductToCategoryUi();
                    break;
            }
        } while (option != 0);

    }

    private void viewAddProductToCategoryUi() {
        System.out.println("Enter id of product:");
        int productId = scanner.nextInt();
        System.out.println("Enter assigned category id:");
        int categoryId = scanner.nextInt();
        scanner.nextLine();
        categoryService.addProductToCategory(productId, categoryId);
    }

    private void viewCategoriesUi() {
        List<Category> categoryList = categoryService.getCategories();
        for (Category category : categoryList) {
            System.out.println(category.getId() + "." + category.getName());
        }
    }

    private void addCategoryUi() {
        System.out.println("Enter category name:");
        String name = scanner.nextLine();
        Category category = new Category();
        category.setName(name);
        categoryService.addCategory(category);
    }
}
