package ui;

import service.ProductService;

import java.util.Scanner;

public class StockUi {

    private ProductService productService;
    private Scanner scanner;

    public StockUi(ProductService productService) {
        this.productService = productService;
    }

    public void startStockUi() {
        scanner = new Scanner(System.in);
        System.out.println("Stock Management");
        int optiune;
        do {
            System.out.println("1.Add stock");
            System.out.println("0.Exit");
            optiune = scanner.nextInt();
            switch (optiune){
                case 1:
                    addStockUi();
                    break;
            }
        } while (optiune != 0);
    }

    private void addStockUi() {
        System.out.println("Enter id of product:");
        int productId = scanner.nextInt();
        System.out.println("Enter incoming stock:");
        int incomingStock = scanner.nextInt();
        productService.addStock(productId, incomingStock);
    }
}
