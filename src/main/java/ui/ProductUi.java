package ui;

import persistence.model.Product;
import service.InvalidIdException;
import service.InvalidPriceException;
import service.ProductService;

import java.util.List;
import java.util.Scanner;

public class ProductUi {


    private Scanner scanner;
    private ProductService productService;

    public ProductUi(ProductService productService) {
        this.productService = productService;
    }


    public void startUi() {
        System.out.println("Product Management");
        scanner = new Scanner(System.in);

        int option;
        do {
            System.out.println("1.Add product");
            System.out.println("2.View products");
            System.out.println("3.Delete product");
            System.out.println("4.Edit product price");
            System.out.println("0.Exit");
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    addProductUi();
                    break;
                case 2:
                    viewProductsUi();
                    break;
                case 3:
                    deleteProductUi();
                    break;
                case 4:
                    editProductPriceUi();
                    break;
            }
        } while (option != 0);
    }

    private void editProductPriceUi() {
        System.out.println("Enter id of product:");
        int productId = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Enter new price of product");
        double price = scanner.nextDouble();
        scanner.nextLine();
        productService.editProductPrice(productId, price);
    }

    private void deleteProductUi() {
        System.out.println("Enter id of product that will be deleted:");
        int idProduct = scanner.nextInt();
        scanner.nextLine();
        try {
            productService.deleteProduct(idProduct);
        } catch (InvalidIdException iie) {
            System.out.println(iie.getMessage());
        }
    }

    private void viewProductsUi() {
        List<Product> products = productService.getProducts();
        for (Product product : products) {
            System.out.println(product.getId() + "." + product.getName() +
                    "<" + product.getDescription() + "> " + product.getPrice() + " lei");
        }
    }

    public void addProductUi() {
        System.out.println("Enter name of product:");
        String productName = scanner.nextLine();
        System.out.println("Enter description of product");
        String description = scanner.nextLine();
        System.out.println("Enter price of product");
        double price = scanner.nextDouble();
        scanner.nextLine();

        Product product = new Product();
        product.setName(productName);
        product.setDescription(description);
        product.setPrice(price);

        try {
            productService.addProduct(product);
        } catch (InvalidPriceException e) {
            System.out.println(e.getMessage());
        }
    }
}
