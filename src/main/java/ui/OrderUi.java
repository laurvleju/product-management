package ui;

import persistence.model.Cart;
import persistence.model.Category;
import persistence.model.Product;
import service.CartService;
import service.CategoryService;
import service.OutOfStockException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OrderUi {

    private CategoryService categoryService;
    private CartService cartService;
    private Scanner scanner;

    public OrderUi(CategoryService categoryService,
                   CartService cartService) {
        this.categoryService = categoryService;
        this.cartService  = cartService;
    }

    public void startOrderUi() {
        scanner = new Scanner(System.in);
        int option;
        do {
            System.out.println("1.Add to cart");
            System.out.println("2.View cart");
            System.out.println("3.Checkout");
            System.out.println("0.Exit");
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    addToCartUi();
                    break;
                case 2:
                    viewCartUi();
                    break;
                case 3:
                    checkoutUi();
                    break;
            }
        } while (option != 0);
    }

    private void checkoutUi() {
        viewCartUi();
        System.out.println("Proceed to checkout ? 1.Yes,2.No");
        int answer = scanner.nextInt();
        if(answer == 1){
            cartService.checkOut();
        }
    }

    private void viewCartUi() {
        Cart cart = cartService.getCart();
        if(cart == null){
            return;
        }
        List<Product> productList = cart.getProductList();
        for(Product product : productList){
            System.out.println(product.getId() + "." +  product.getName() + " pret:"+
                    product.getPrice() + " lei");
        }
        System.out.println("-----------------------------");
        System.out.println("TOTAL:" + cart.getTotal() + " lei");
        System.out.println("-----------------------------");

    }

    private void addToCartUi() {
        System.out.println("Chose a category:");
        List<Category> categories = categoryService.getCategories();

        for (Category category : categories) {
            System.out.println(category.getId() + "." + category.getName());
        }
        int categoryId = scanner.nextInt();

        List<Product> products = categoryService.getProductsInCategory(categoryId);
        for (Product product : products) {
            System.out.println(product.getId() + "." + product.getName() +
                    " price:" + product.getPrice() + " lei");
        }
        System.out.println("Chose a product:");
        int productId = scanner.nextInt();
        try {
            cartService.addToCart(productId);
        } catch (OutOfStockException e) {
            System.out.println(e.getMessage());
        }
    }
}
