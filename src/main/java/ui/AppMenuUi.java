package ui;

import org.hibernate.criterion.Order;

import java.util.Scanner;

public class AppMenuUi {

    private ProductUi productUi;
    private CategoryUi categoryUi;
    private StockUi stockUi;
    private OrderUi orderUi;

    public AppMenuUi(ProductUi productUi, CategoryUi categoryUi,
                     StockUi stockUi, OrderUi orderUi) {
        this.productUi = productUi;
        this.categoryUi = categoryUi;
        this.stockUi = stockUi;
        this.orderUi = orderUi;
    }

    public void startUi() {

        Scanner scanner = new Scanner(System.in);
        int option;
        do {
            System.out.println("1.Product management");
            System.out.println("2.Category management");
            System.out.println("3.Stock management");
            System.out.println("4.Order management");
            System.out.println("0.Exit");
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    productUi.startUi();
                    break;
                case 2:
                    categoryUi.startCategoryUi();
                    break;
                case 3:
                    stockUi.startStockUi();
                    break;
                case 4:
                    orderUi.startOrderUi();
                    break;
            }
        } while (option != 0);
    }
}
