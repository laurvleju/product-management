package service;

import persistence.model.Product;
import persistence.ProductDao;

import java.util.List;

public class ProductService {

    private ProductDao productDao;

    public ProductService(ProductDao productDao) {
        this.productDao = productDao;
    }

    public void addProduct(Product product) throws InvalidPriceException {

        if (product.getPrice() > 0) {
            productDao.save(product);
        } else {
            throw new InvalidPriceException();
        }
    }

    public List<Product> getProducts(){
        List<Product> products = productDao.findAll();
        return products;
    }

    //50
    public void deleteProduct(int id){
        Product product = productDao.findById(id);
        if(product == null){
            throw new InvalidIdException();
        }
        productDao.delete(product);
    }

    public void editProductPrice(int id, double newPrice){
        Product product =  productDao.findById(id);
        product.setPrice(newPrice);
        productDao.save(product);
    }


    // idProdus : 1 // stockPrimit 20;

    public void addStock(int idProduct, int incommingStock){
        Product product = productDao.findById(idProduct);
        int stockActual = product.getStock(); // 5
        product.setStock(stockActual + incommingStock);
        productDao.save(product);
    }
}
