package service;

public class InvalidPriceException extends Exception {

    public InvalidPriceException(){
        super("Price is lower then 0!");
    }
}
