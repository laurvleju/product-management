package service;

import persistence.ProductDao;
import persistence.model.Category;
import persistence.CategoryDao;
import persistence.model.Product;

import java.util.List;

public class CategoryService {

    private CategoryDao categoryDao;

    private ProductDao productDao;

    public CategoryService(CategoryDao categoryDao, ProductDao productDao) {
        this.categoryDao = categoryDao;
        this.productDao = productDao;
    }

    public void addCategory(Category category) {
        categoryDao.save(category);
    }

    public List<Category> getCategories() {
        List<Category> categories = categoryDao.findAll();
        return categories;
    }

    public void addProductToCategory(int productId, int categoryId) {
        Product productFound = productDao.findById(productId);
        Category categoryFound = categoryDao.findById(categoryId);
        productFound.setCategory(categoryFound);
        productDao.save(productFound);
    }

    // 2
    public List<Product> getProductsInCategory(int categoryId) {
        Category category = categoryDao.findById(categoryId);
        List<Product> productsInCategory = category.getProducts();
        return productsInCategory;
    }
}
