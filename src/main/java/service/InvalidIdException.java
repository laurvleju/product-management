package service;

public class InvalidIdException extends RuntimeException {

    public InvalidIdException(){
        super("Invalid id!");
    }
}
