package service;

import persistence.CartDao;
import persistence.ProductDao;
import persistence.model.Cart;
import persistence.model.Product;

import java.util.List;

public class CartService {

    private CartDao cartDao;

    private ProductDao productDao;

    public CartService(CartDao cartDao, ProductDao productDao) {
        this.cartDao = cartDao;
        this.productDao = productDao;
    }


    //idProdus idCaruciorul
    public void addToCart(int productId) throws OutOfStockException {
        Product product = productDao.findById(productId);

        if (product.getStock() < 1) {
            throw new OutOfStockException();
        }
        Cart cart = cartDao.findById(1);
        if (cart == null) {
            cart = new Cart();
            cart.setId(1);
            cartDao.save(cart);
        }
        List<Product> productsInCart = cart.getProductList();
        productsInCart.add(product);

        double sum = 0;
        for (Product prod : productsInCart) {
            sum = sum + prod.getPrice();
        }
        cart.setTotal(sum);
        cartDao.update(cart);
    }

    public Cart getCart() {
        Cart cart = cartDao.findById(1);
        return cart;
    }


    public void checkOut(){
        Cart cart = cartDao.findById(1);
        List<Product> products = cart.getProductList();
        for(Product product : products){
            int currentStock = product.getStock();
            product.setStock(currentStock - 1);
            productDao.save(product);
        }
        cartDao.delete(cart);
    }
}
