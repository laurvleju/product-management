package service;

public class OutOfStockException extends Exception {

    public OutOfStockException(){
        super("Out of stock exception");
    }
}
