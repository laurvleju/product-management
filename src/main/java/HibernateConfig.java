import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import persistence.model.Cart;
import persistence.model.Category;
import persistence.model.Product;

import java.util.Properties;

public class HibernateConfig {

    private SessionFactory sessionFactory;

    public HibernateConfig() {

        Configuration configuration = new Configuration();
        Properties properties = new Properties();
        properties.put(Environment.URL, "jdbc:mysql://localhost:3306/productmanagement");
        properties.put(Environment.USER, "root");
        properties.put(Environment.PASS, "Diavolo123");
        properties.put(Environment.DRIVER, "com.mysql.jdbc.Driver");

        properties.put(Environment.HBM2DDL_AUTO, "update");

        properties.put(Environment.SHOW_SQL, true);


        configuration.setProperties(properties);
        configuration.addAnnotatedClass(Product.class);
        configuration.addAnnotatedClass(Category.class);
        configuration.addAnnotatedClass(Cart.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(properties).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
