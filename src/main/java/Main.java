import org.hibernate.SessionFactory;
import persistence.CartDao;
import persistence.CategoryDao;
import persistence.ProductDao;
import service.CartService;
import service.CategoryService;
import service.ProductService;
import ui.*;

public class Main {

    public static void main(String[] args) {
        HibernateConfig hibernateConfig = new HibernateConfig();

        SessionFactory sessionFactory = hibernateConfig.getSessionFactory();
        ProductDao productDao = new ProductDao(sessionFactory);
        ProductService productService = new ProductService(productDao);
        ProductUi productUi = new ProductUi(productService);

        CategoryDao categoryDao = new CategoryDao(sessionFactory);
        CategoryService categoryService = new CategoryService(categoryDao, productDao);
        CategoryUi categoryUi = new CategoryUi(categoryService);


        StockUi stockUi = new StockUi(productService);

        CartDao cartDao = new CartDao(sessionFactory);
        CartService cartService = new CartService(cartDao, productDao);
        OrderUi orderUi = new OrderUi(categoryService, cartService);
        AppMenuUi appMenuUi = new AppMenuUi(productUi, categoryUi, stockUi, orderUi);
        appMenuUi.startUi();
    }
}
