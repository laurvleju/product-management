package persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import persistence.model.Product;

import java.util.List;
//CRUD -> Create(Save) R (findAll findById) U (update) D (delete)
public class ProductDao {

    private SessionFactory sessionFactory;


    public ProductDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Product product) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(product);
        transaction.commit();
        session.close();
    }

    public List<Product> findAll() {
        Session session = sessionFactory.openSession();
        List<Product> products = session.createQuery("from Product").getResultList();
        session.close();
        return products;
    }

    public void delete(Product product) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(product);
        transaction.commit();
        session.close();
    }

    public Product findById(int id) {
        Session session = sessionFactory.openSession();
        Product product = session.find(Product.class, id);
        session.close();
        return product;
    }
}
