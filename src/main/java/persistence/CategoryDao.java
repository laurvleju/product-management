package persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import persistence.model.Category;

import java.util.List;

public class CategoryDao {

    private SessionFactory sessionFactory;

    public CategoryDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Category category) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(category);
        transaction.commit();
        session.close();
    }

    public List<Category> findAll() {
        Session session = sessionFactory.openSession();
        List<Category> categories = session.createQuery("from Category").getResultList();
        session.close();
        return categories;
    }

    public Category findById(int categoryId) {
        Session session = sessionFactory.openSession();
        Category category = session.find(Category.class, categoryId);
        session.close();
        return category;
    }
}
