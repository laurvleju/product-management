package persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import persistence.model.Cart;

public class CartDao {

    private SessionFactory sessionFactory;

    public CartDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Cart findById(int idCart) {
        Session session = sessionFactory.openSession();
        Cart cart = session.find(Cart.class, idCart);
        session.close();
        return cart;
    }


    public void update(Cart cart) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        //
        session.update(cart);
        transaction.commit();
        session.close();
    }

    public void save(Cart cart) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        //
        session.save(cart);
        transaction.commit();
        session.close();
    }

    public void delete(Cart cart) {
        Session session  = sessionFactory.openSession();
        Transaction transaction =  session.beginTransaction();
        session.delete(cart);
        transaction.commit();
        session.close();
    }
}
