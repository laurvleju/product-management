package persistence.model;
import persistence.model.Product;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


// ebtitate din tabel(se va crea un tabel cu coloanele ca fiin drept membrii clasei curente)
@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    //produsele ce apartin unei categorii (categoriei curente)
    @OneToMany(mappedBy = "category")
    private List<Product> products = new ArrayList<Product>();


    public int getId() {
        return id;
    }

    public List<Product> getProducts() {

        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
